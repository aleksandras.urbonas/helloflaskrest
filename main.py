# title  : hello REST FLask
# version: 2022-02-17T1941 AUR
# review : 2022-02-19T1044 AUR


import sqlite3 as sql
from flask import Flask, render_template, request


import sqlite3

conn = sqlite3.connect('database.db')
print("Opened database successfully")

conn.execute('CREATE TABLE IF NOT EXISTS students (name TEXT, addr TEXT, city TEXT, pin TEXT)')
print("Table created successfully")
conn.close()
print("DB closed")


app = Flask(
    __name__
    , static_folder='static'
    , template_folder='static/templates'
)


@app.route('/')
def home():
    return render_template('home.html')


@app.route('/enternew')
def new_student():
    return render_template('student.html')


@app.route('/addrec', methods=['POST', 'GET'])
def addrec():
    if request.method == 'POST':
        try:
            nm = request.form['nm']
            addr = request.form['add']
            city = request.form['city']
            pin = request.form['pin']

            print(f"""
            nm  : {nm}
            addr: {addr}
            city: {city}
            PIN : {pin}
            """
            )

            with sql.connect("database.db") as con:
                cur = con.cursor()

                cur.execute("""
                INSERT INTO students (name,addr,city,pin)
                VALUES(?, ?, ?, ?)
                """
                , (nm, addr, city, pin)
                )

                con.commit()
                msg = "Record successfully added"
        except Exception as e:
            con.rollback()

            ### review: 20220219 AU
            msg = str(e)
            # msg = "error in insert operation" + str(e.code)
        finally:
            return render_template("result.html", msg=msg)
            con.close()


@app.route('/list')
def list():
    con = sql.connect("database.db")
    con.row_factory = sql.Row

    cur = con.cursor()
    cur.execute("SELECT * FROM students LIMIT 10")

    rows = cur.fetchall()
    return render_template("list.html", rows=rows)


if __name__ == '__main__':
    print("flask test:\n\n")
    app.run(debug=True)
